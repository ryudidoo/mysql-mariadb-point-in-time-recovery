# mysql-mariadb-point-in-time-recovery

Point in Time recovery for MySQL & MariaDB

# Galera Cluster advise

To safely be able to do point in time recovery from a Galera cluster you will need to run point in time recovery from each node. This is due to the SST process overwriting the binary logs.

# Example Ansible play

```
 - hosts: db-binlog
    become: True
    roles:
      - role: db-point-in-time-recovery
        vars:
          master_hostname: "{{ (groups['db-cluster'] | map('extract', hostvars, ['ansible_host']) | list)[0] }}"
          replication_password: "{{ vault_binlog_pitr_password }}"
          binlog_base_dir: "/data/"
          maxscale_listen_port: 6001
          maxscale_admin_port: 8001
        tags: db-point-in-time-recovery-source
  
  - hosts: db-binlog
    become: True
    roles:
      - role: db-point-in-time-recovery
        vars:
          master_hostname: "{{ (groups['db-cluster'] | map('extract', hostvars, ['ansible_host']) | list)[1] }}"
          replication_password: "{{ vault_binlog_pitr_password }}"
          binlog_base_dir: "/data/"
          maxscale_listen_port: 6002
          maxscale_admin_port: 8002
        tags: db-point-in-time-recovery-source
  
  - hosts: db-binlog
    become: True
    roles:
      - role: db-point-in-time-recovery
        vars:
          master_hostname: "{{ (groups['db-cluster'] | map('extract', hostvars, ['ansible_host']) | list)[2] }}"
          replication_password: "{{ vault_binlog_pitr_password }}"
          binlog_base_dir: "/data/"
          maxscale_listen_port: 6003
          maxscale_admin_port: 8003
        tags: db-point-in-time-recovery-source
```


# LICENSE RESTRICTIONS
MariaDB's MaxScale is not open-source. It is under their proprietary Business Source License (BS License). The license has a free usage section called 'Additional Use Grant': `You may use the Licensed Work when your application uses the Licensed Work with a total of less than three server instances for any purpose.` - this role only set up MaxScale binlog server to connect to 1 server instance per running MaxScale for collecting the binary logs, and 1 other server instance for recovering the binary logs. Therefore the Licensed Work is not connecting to more then 2 server instances.