- name: "Make sure config directory exists"
  file:
    path: "{{ config_dir }}"
    state: directory
    recurse: yes

- name: "Make sure log directory exists"
  file:
    path: "{{ log_dir }}"
    state: directory
    owner: maxscale
    group: maxscale
    recurse: yes

- name: "Deploy MaxScale configuration"
  template:
    src: "templates/maxscale.cnf.j2"
    dest: "/etc/maxscale/{{ master_hostname }}.cnf"
  notify: restart maxscale

- name: "Create data directory"
  file:
    path: "{{ data_dir }}"
    state: directory
    owner: maxscale
    group: maxscale
    recurse: yes

- name: "Deploy systemd service template"
  template:
    src: "maxscale.service.j2"
    dest: "/etc/systemd/system/maxscale@.service"

- name: "Start and enable the service"
  systemd:
    daemon_reload: yes
    name: "maxscale@{{ master_hostname }}"
    state: started
    enabled: yes

- name: "Check if slave already running - Get replication status from MaxScale"
  mysql_replication:
    login_host: "127.0.0.1"
    login_port: "{{ maxscale_listen_port }}"
    login_user: "{{ replication_user }}"
    login_password: "{{ replication_password }}"
    mode: getslave
  register: maxscale_slave_status
  failed_when: false

- name: "SHOW SLAVE STATUS"
  debug:
    var: maxscale_slave_status

- when: not maxscale_slave_status | default(False) or
   not maxscale_slave_status['Slave_IO_Running'] | default(False) == 'Yes' or
   not maxscale_slave_status['Slave_SQL_Running'] | default(False) == 'Yes' 
  block:

  - name: "Reset replication from the master to maxscale"
    mysql_replication:
      mode: resetslave
      login_host: "127.0.0.1"
      login_port: "{{ maxscale_listen_port }}"
      login_user: "{{ replication_user }}"
      login_password: "{{ replication_password }}"
  
  - name: "Set up replication from the master to maxscale"
    mysql_replication:
      mode: changemaster
      login_host: "127.0.0.1"
      login_port: "{{ maxscale_listen_port }}"
      login_user: "{{ replication_user }}"
      login_password: "{{ replication_password }}"
      master_host: "{{ master_hostname }}"
      master_port: "{{ master_port }}"
      master_user: "{{ replication_user }}"
      master_password: "{{ replication_password }}"
      master_log_file: "{{ master_first_binlog }}"
      master_log_pos: 4
  
  - name: "Start replication from the master to maxscale"
    mysql_replication:
      mode: startslave
      login_host: "127.0.0.1"
      login_port: "{{ maxscale_listen_port }}"
      login_user: "{{ replication_user }}"
      login_password: "{{ replication_password }}"
