- name: "Determining replication user hostname through inventory_hostname"
  set_fact:
    replication_client_host: "{{ ansible_default_ipv4.address }}"
  when: replication_client_host is undefined


# Not supported yet, must also update ini file
#  - when: random_replication_password and replication_password is undefined
#  block:
#  - name: "Generating a random replication password"
#    shell: |
#      python -c 'import string; import random; print "".join(random.choice(string.ascii_letters + string.digits + "!@#$*!@#*") for x in range(32));'
#    register: random_password_command
#
#  - name: "Fetching random password from command"
#    set_fact:
#      replication_password: "{{ random_password_command.stdout_lines[0] }}"
#
#- name: "Generated random replication user password"
#  debug:
#    var: replication_password
#    verbosity: 3
#  when: random_replication_password | bool

- name: "Create replication user on master"
  mysql_user:
    name: "{{ replication_user }}"
    host: "{{ replication_client_host }}"
    password: "{{ replication_password }}"
    login_user: "{{ login_user }}"
    login_password: "{{ login_password }}"
    priv: '*.*:REPLICATION SLAVE'
    state: present
  delegate_to: "{{ master_hostname }}"

- name: "Get master's server_id from config"
  shell: "grep -hriI server_id /etc/my*|tr -d ' ' > /tmp/get-server_id && . /tmp/get-server_id && echo $server_id && rm -f /tmp/get-server_id" # Ansible trims empty lines from the result
  register: master_server_id_from_config
  delegate_to: "{{ master_hostname }}"

- name: "Storing master server id"
  set_fact:
    master_server_id: "{{ master_server_id_from_config.stdout_lines[0] }}"
  when:
    - master_server_id_from_config.stdout_lines
    - master_server_id_from_config.stdout_lines[0] != ""

- name: "Verify server id is set up (correctly) on both ends"
  assert:
    that:
      - maxscale_server_id is defined
      - master_server_id is defined
      - maxscale_server_id != master_server_id
    msg: "Please configure server_id on both ends (master and point in time recovery role), they cannot be the same"

- name: "Storing current data and binlog directories"
  set_fact:
    master_datadir: "/var/lib/mysql"
    master_binlog_dir: false
    master_binlog_basename: false

- name: "Get current data directory from running MariaDB instance"
  shell: "mysql -e 'SHOW GLOBAL VARIABLES LIKE \"datadir\"' |grep datadir|awk '{print $2}'"
  register: master_datadir_from_running_process
  delegate_to: "{{ master_hostname }}"

- name: "Get current data directory from config"
  shell: "grep -hri datadir /etc/my.cnf*|tr -d ' ' > /tmp/get-datadir && . /tmp/get-datadir && echo $datadir && rm -f /tmp/get-datadir"
  register: master_datadir_from_config
  delegate_to: "{{ master_hostname }}"

- name: "Storing current data directory - system is running (extracted from config file)"
  set_fact:
    master_datadir: "{{ master_datadir_from_running_process.stdout_lines[0] }}"
  when: 
    - master_datadir_from_running_process.rc == 0
    - master_datadir_from_running_process.stdout_lines
  delegate_to: "{{ master_hostname }}"

- name: "Storing current data directory - system is not running"
  set_fact:
    master_datadir: "{{ master_datadir_from_config.stdout_lines[0] }}"
  when: 
    - (master_datadir_from_running_process.rc != 0 or not master_datadir_from_running_process.stdout_lines)
    - master_datadir_from_config.stdout_lines
  delegate_to: "{{ master_hostname }}"

- name: "Get current binlog directory from running MariaDB instance"
  shell: "mysql -e 'SHOW GLOBAL VARIABLES LIKE \"log_bin_basename\"' |grep log_bin_basename|awk '{print $2}'"
  register: master_binlogdir_from_running_process
  delegate_to: "{{ master_hostname }}"

- name: "Get current binlog directory from config"
  shell: "grep -hri log_bin /etc/my.cnf*|tr -d ' ' > /tmp/get-binlog_dir && . /tmp/get-binlog_dir && echo $log_bin && rm -f /tmp/get-binlog_dir" # Ansible trims empty lines from the result
  register: master_binlogdir_from_config
  delegate_to: "{{ master_hostname }}"

- name: "Storing current binlog directory - system is running"
  set_fact:
    master_binlog_dir: "{{ master_binlogdir_from_running_process.stdout_lines[0] | dirname }}"
    master_binlog_basename: "{{ master_binlogdir_from_running_process.stdout_lines[0] | basename}}"
  when: 
    - master_binlogdir_from_running_process.rc == 0
    - master_binlogdir_from_running_process.stdout_lines

- name: "Storing current binlog directory - system is not running (extracted log_bin from config file)"
  set_fact:
    master_binlog_dir: "{{ master_binlogdir_from_config.stdout_lines[0] | dirname }}"
    master_binlog_basename: "{{ master_binlogdir_from_config.stdout_lines[0] |basename }}"
  when: 
    - (master_binlogdir_from_running_process.rc != 0 or not master_binlogdir_from_running_process.stdout_lines)
    - master_binlogdir_from_config.stdout_lines
    - master_binlogdir_from_config.stdout_lines[0] != ""
  
- name: "Remove trailing slashes from relevant facts"
  set_fact:
    master_binlog_dir: "{{ master_binlog_dir | regex_replace('\\/$', '') }}"
    master_datadir: "{{ master_datadir | regex_replace('\\/$', '') }}"

- name: "Print current MariaDB data dir"
  debug:
    msg: "Determined current data directory to be {{ master_datadir }}"

- name: "Print current binlog directory"
  debug:
    msg: "Determined current binlog directory and basename to be {{ master_binlog_dir }} / {{ master_binlog_basename }}"

- name: "Find the first binlog on master"
  shell: "ls {{ master_binlog_dir }}/{{ master_binlog_basename }}.*|sort|head -1"
  register: binlog_shell_command
  delegate_to: "{{ master_hostname }}"

- name: "Store first binlog in fact"
  set_fact:
    master_first_binlog: "{{ binlog_shell_command.stdout_lines[0] | basename }}"

- name: "Print first binlog file"
  debug:
    msg: "Determined current binlog file to be {{ master_first_binlog }}"
